#include "pch.h"
#include <iostream>

#include <stdlib.h>
#include <stdio.h>
#include <math.h> 
#include <vector>

#include <GL/glew.h>

#define GLM_FORCE_CTOR_INIT 
#include <GLM.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <glfw3.h>

#include <iostream>
#include <fstream>
#include <sstream>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#pragma comment (lib, "glfw3dll.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

// settings
const unsigned int SCR_WIDTH = 1500;
const unsigned int SCR_HEIGHT = 900;
float vka = 0;
float vkd = 0;
float exponent = 2.0;

enum ECameraMovementType
{
	UNKNOWN,
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

class Camera
{
private:
	// Default camera values
	const float zNEAR = 0.1f;
	const float zFAR = 100.f;
	const float YAW = -90.0f;
	const float PITCH = 0.0f;
	const float FOV = 45.0f;
	glm::vec3 startPosition;

public:
	Camera(const int width, const int height, const glm::vec3 &position)
	{
		startPosition = position;
		Set(width, height, position);
	}

	void Set(const int width, const int height, const glm::vec3 &position)
	{
		this->isPerspective = true;
		this->yaw = YAW;
		this->pitch = PITCH;

		this->FoVy = FOV;
		this->width = width;
		this->height = height;
		this->zNear = zNEAR;
		this->zFar = zFAR;

		this->worldUp = glm::vec3(0, 1, 0);
		this->position = position;

		lastX = width / 2.0f;
		lastY = height / 2.0f;
		bFirstMouseMove = true;

		UpdateCameraVectors();
	}

	void Reset(const int width, const int height)
	{
		Set(width, height, startPosition);
	}

	void Reshape(int windowWidth, int windowHeight)
	{
		width = windowWidth;
		height = windowHeight;

		// define the viewport transformation
		glViewport(0, 0, windowWidth, windowHeight);
	}

	const glm::mat4 GetViewMatrix() const
	{
		// Returns the View Matrix
		return glm::lookAt(position, position + forward, up);
	}

	const glm::vec3 GetPosition() const
	{
		return position;
	}

	const glm::mat4 GetProjectionMatrix() const
	{
		glm::mat4 Proj = glm::mat4(1);
		if (isPerspective) {
			float aspectRatio = ((float)(width)) / height;
			Proj = glm::perspective(glm::radians(FoVy), aspectRatio, zNear, zFar);
		}
		else {
			float scaleFactor = 2000.f;
			Proj = glm::ortho<float>(
				-width / scaleFactor, width / scaleFactor,
				-height / scaleFactor, height / scaleFactor, -zFar, zFar);
		}
		return Proj;
	}

	bool verificaColiziune(glm::vec3 position, glm::vec3 oldPosition, float xm, float xM, float zm, float zM)
	{
		float x1, x2, z1, z2;
		if (position.x < oldPosition.x)
		{
			x1 = position.x;
			x2 = oldPosition.x;
		}
		else
		{
			x1 = oldPosition.x;
			x2 = position.x;
		}

		if (position.z < oldPosition.z)
		{
			z1 = position.z;
			z2 = oldPosition.z;
		}
		else
		{
			z1 = oldPosition.z;
			z2 = position.z;
		}

		return ((x1 <= xM && x2 >= xm) && (z1 <= zM && z2 >= zm));
	}

	void ProcessKeyboard(ECameraMovementType direction, float deltaTime)
	{
		std::vector<float> wallsCoord =
		{
		60.0f, -0.5f, -20.0f,
		60.0f, -0.5f, 60.0f,
		60.0f, 2.5f, 60.0f,

		60.0f, -0.5f, -20.0f,
		60.0f, 2.5f, 60.0f,
		60.0f, 2.5f, -20.0f,

		-20.0f, -0.5f, 60.0f,
		-20.0f, -0.5f, -20.0f,
		-20.0f, 2.5f, -20.0f,

		-20.0f, -0.5f, 60.0f,
		-20.0f, 2.5f, -20.0f,
		-20.0f, 2.5f, 60.0f,

		18.0f, -0.5f, 60.0f,
		-20.0f, -0.5f, 60.0f,
		-20.0f, 2.5f, 60.0f,

		18.0f, -0.5f, 60.0f,
		-20.0f, 2.5f, 60.0f,
		18.0f, 2.5f, 60.0f,

		60.0f, -0.5f, 60.0f,
		22.0f, -0.5f, 60.0f,
		22.0f, 2.5f, 60.0f,

		60.0f, -0.5f, 60.0f,
		22.0f, 2.5f, 60.0f,
		60.0f, 2.5f, 60.0f,

		60.0f, -0.5f, -20.0f,
		22.0f, -0.5f, -20.0f,
		22.0f, 2.5f, -20.0f,

		60.0f, -0.5f, -20.0f,
		22.0f, 2.5f, -20.0f,
		60.0f, 2.5f, -20.0f,

		18.0f, -0.5f, -20.0f,
		-20.0f, -0.5f, -20.0f,
		-20.0f, 2.5f, -20.0f,

		18.0f, -0.5f, -20.0f,
		-20.0f, 2.5f, -20.0f,
		18.0f, 2.5f, -20.0f,

		22.0f, -0.5f,  60.0f,
		22.0f, -0.5f,  56.0f, 
		22.0f, 2.5f,  60.0f,  

		22.0f, 2.5f,  60.0f,  
		22.0f, 2.5f,  56.0f,  
		22.0f, -0.5f, 56.0f,  


		22.0f, -0.5f,  56.0f, 
		12.0f, -0.5f,  56.0f, 
		22.0f,  2.5f,  56.0f, 

		22.0f, 2.5f,  56.0f,  
		12.0f, 2.5f,  56.0f,  
		12.0f,  -0.5f, 56.0f, 

		19.0f, -0.5f, 56.0f,  
		19.0f, -0.5f, 50.0f, 
		19.0f, 2.5f, 50.0f,  

		19.0f, -0.5f, 56.0f, 
		19.0f, 2.5f, 50.0f,  
		19.0f, 2.5f, 56.0f,  

		6.0f, -0.5f, 60.0f,  
		6.0f, -0.5f, 50.0f,  
		6.0f, 2.5f, 50.0f,   

		6.0f, -0.5f, 60.0f,  
		6.0f, 2.5f, 50.0f,   
		6.0f, 2.5f, 60.0f,   

		24.0f, -0.5f, 60.0f,  
		24.0f, -0.5f, 38.0f,  
		24.0f, 2.5f, 38.0f,   

		24.0f, -0.5f, 60.0f,  
		24.0f, 2.5f, 38.0f,   
		24.0f, 2.5f, 60.0f,   

		24.0f, -0.5f, 38.0f,  
		14.0f, -0.5f, 38.0f,  
		14.0f, 2.5f, 38.0f,   

		24.0f, -0.5f, 38.0f,  
		14.0f, 2.5f, 38.0f,   
		24.0f, 2.5f, 38.0f,   

		10.0f, -0.5f, 38.0f,  
		-20.0f, -0.5f, 38.0f, 
		-20.0f, 2.5f, 38.0f,  

		10.0f, -0.5f, 38.0f,  
		-20.0f, 2.5f, 38.0f,  
		10.0f, 2.5f, 38.0f,   

		12.0f, -0.5f, 44.0f,  
		-8.0f, -0.5f, 44.0f,  
		-8.0f, 2.5f, 44.0f,   

		12.0f, -0.5f, 44.0f,  
		-8.0f, 2.5f, 44.0f,   
		12.0f, 2.5f, 44.0f,   

		36.0f, -0.5f, 26.0f,  
		-20.0f, -0.5f, 26.0f, 
		-20.0f, 2.5f, 26.0f,  

		36.0f, -0.5f, 26.0f,  
		-20.0f, 2.5f, 26.0f,  
		36.0f, 2.5f, 26.0f,   

		18.0f, -0.5f, 26.0f,  
		18.0f, -0.5f, 2.0f,   
		18.0f, 2.5f, 2.0f,    

		18.0f, -0.5f, 26.0f,  
		18.0f, 2.5f, 2.0f,    
		18.0f, 2.5f, 26.0f,   

		26.0f, -0.5f, 2.0f,   
		18.0f, -0.5f, 2.0f,   
		18.0f, 2.5f, 2.0f,    

		26.0f, -0.5f, 2.0f,   
		18.0f, 2.5f, 2.0f,    
		26.0f, 2.5f, 2.0f,    

		30.0f, -0.5f, 54.0f, 
		30.0f, -0.5f, 48.0f, 
		30.0f, 2.5f, 48.0f,  

		30.0f, -0.5f, 54.0f, 
		30.0f, 2.5f, 48.0f,  
		30.0f, 2.5f, 54.0f,  

		36.0f, -0.5f, 54.0f, 
		30.0f, -0.5f, 54.0f, 
		30.0f, 2.5f, 54.0f,  

		36.0f, -0.5f, 54.0f, 
		30.0f, 2.5f, 54.0f,  
		36.0f, 2.5f, 54.0f,  

		44.0f, -0.5f, 60.0f, 
		44.0f, -0.5f, 56.0f, 
		44.0f, 2.5f, 56.0f,  

		44.0f, -0.5f, 60.0f, 
		44.0f, 2.5f, 56.0f,  
		44.0f, 2.5f, 60.0f,  

		30.0f, -0.5f, 42.0f, 
		30.0f, -0.5f, 36.0f, 
		30.0f, 2.5f, 36.0f,  

		30.0f, -0.5f, 42.0f, 
		30.0f, 2.5f, 36.0f,  
		30.0f, 2.5f, 42.0f,  

		40.0f, -0.5f, 42.0f, 
		30.0f, -0.5f, 42.0f, 
		30.0f, 2.5f, 42.0f,  

		40.0f, -0.5f, 42.0f, 
		30.0f, 2.5f, 42.0f,  
		40.0f, 2.5f, 42.0f,  

		40.0f, -0.5f, 50.0f, 
		40.0f, -0.5f, 42.0f, 
		40.0f, 2.5f, 42.0f,  

		40.0f, -0.5f, 50.0f, 
		40.0f, 2.5f,  42.0f, 
		40.0f, 2.5f, 50.0f,  

		55.0f, -0.5f, 50.0f, 
		40.0f, -0.5f, 50.0f, 
		40.0f, 2.5f, 50.0f,  

		55.0f, -0.5f, 50.0f, 
		40.0f, 2.5f, 50.0f,  
		55.0f, 2.5f, 50.0f,  

		50.0f, -0.5f, 50.0f, 
		50.0f, -0.5f, 8.0f,  
		50.0f, 2.5f, 8.0f,   

		50.0f, -0.5f, 50.0f, 
		50.0f, 2.5f, 8.0f,   
		50.0f, 2.5f, 50.0f,  

		55.0f, -0.5f, 32.0f, 
		50.0f, -0.5f, 32.0f, 
		50.0f, 2.5f, 32.0f,  

		55.0f, -0.5f, 32.0f, 
		50.0f, 2.5f, 32.0f,  
		55.0f, 2.5f, 32.0f,  

		44.0f, -0.5f, 38.0f, 
		44.0f, -0.5f, 22.0f, 
		44.0f, 2.5f, 22.0f,  

		44.0f, -0.5f, 38.0f, 
		44.0f, 2.5f, 22.0f,  
		44.0f, 2.5f, 38.0f,  

		54.0f, -0.5f, 22.0f, 
		44.0f, -0.5f, 22.0f, 
		44.0f, 2.5f, 22.0f,  

		54.0f, -0.5f, 22.0f, 
		44.0f, 2.5f, 22.0f,  
		54.0f, 2.5f, 22.0f,  

		60.0f, -0.5f, 12.0f, 
		55.0f, -0.5f, 12.0f, 
		55.0f, 2.5f, 12.0f,  

		60.0f, -0.5f, 12.0f, 
		55.0f, 2.5f, 12.0f,  
		60.0f, 2.5f, 12.0f,  

		60.0f, -0.5f, 2.0f, 
		48.0f, -0.5f, 2.0f, 
		48.0f, 2.5f, 2.0f,  

		60.0f, -0.5f, 2.0f, 
		48.0f, 2.5f, 2.0f,  
		60.0f, 2.5f, 2.0f,  

		42.0f, -0.5f, 4.0f,  
		32.0f, -0.5f, 4.0f,  
		32.0f, 2.5f, 4.0f,   

		42.0f, -0.5f, 4.0f,  
		32.0f, 2.5f, 4.0f,   
		42.0f, 2.5f, 4.0f,   

		32.0f, -0.5f, 4.0f,  
		32.0f, -0.5f, 10.0f, 
		32.0f, 2.5f, 10.0f,  

		32.0f, -0.5f, 4.0f,  
		32.0f, 2.5f, 10.0f,  
		32.0f, 2.5f, 4.0f,   

		32.0f, -0.5f, 10.0f, 
		24.0f, -0.5f, 10.0f, 
		24.0f, 2.5f, 10.0f,  

		32.0f, -0.5f, 10.0f, 
		24.0f, 2.5f, 10.0f,  
		32.0f, 2.5f, 10.0f,  

		24.0f, -0.5f, 26.0f, 
		24.0f, -0.5f, 10.0f, 
		24.0f, 2.5f, 10.0f,  

		24.0f, -0.5f, 26.0f, 
		24.0f, 2.5f, 10.0f,  
		24.0f, 2.5f, 26.0f,  

		32.0f, -0.5f, 18.0f, 
		24.0f, -0.5f,18.0f,  
		24.0f, 2.5f, 18.0f,  


		32.0f, -0.5f,18.0f,  
		24.0f, 2.5f, 18.0f,  
		32.0f, 2.5f, 18.0f,  

		40.0f, -0.5f, 18.0f, 
		40.0f, -0.5f, 8.0f,  
		40.0f, 2.5f,  8.0f,  

		40.0f, -0.5f, 18.0f, 
		40.0f, 2.5f,  8.0f,  
		40.0f, 2.5f,  18.0f, 

		-4.0f, -0.5f, 26.0f, 
		-4.0f, -0.5f, 18.0f, 
		-4.0f, 2.5f, 18.0f,  

		-4.0f, -0.5f, 26.0f, 
		-4.0f, 2.5f, 18.0f,  
		-4.0f, 2.5f, 26.0f,  

		8.0f, -0.5f, 18.0f,  
		-4.0f, -0.5f, 18.0f, 
		-4.0f, 2.5f, 18.0f,  

		8.0f, -0.5f, 18.0f, 
		-4.0f, 2.5f, 18.0f, 
		8.0f, 2.5f, 18.0f,  

		8.0f, -0.5f, 18.0f, 
		8.0f, -0.5f, 6.0f,  
		8.0f, 2.5f, 6.0f,   

		8.0f, -0.5f, 18.0f, 
		8.0f, 2.5f, 6.0f,   
		8.0f, 2.5f, 18.0f,  

		-2.0f, -0.5f, 18.0f, 
		-2.0f, -0.5f, 6.0f,  
		-2.0f, 2.5f, 6.0f,   

		-2.0f, -0.5f, 18.0f, 
		-2.0f, 2.5f, 6.0f,   
		-2.0f, 2.5f, 18.0f,  

		-14.0f, -0.5f, 16.0f,
		-14.0f, -0.5f, -4.0f,
		-14.0f, 2.5f, -4.0f, 

		-14.0f, -0.5f, 16.0f,
		-14.0f, 2.5f, -4.0f, 
		-14.0f, 2.5f, 16.0f, 

		-8.0f, -0.5f, 16.0f, 
		-14.0f, -0.5f, 16.0f,
		-14.0f, 2.5f, 16.0f, 

		-8.0f, -0.5f, 16.0f, 
		-14.0f, 2.5f, 16.0f, 
		-8.0f, 2.5f, 16.0f,  

		-8.0f, -0.5f, 16.0f, 
		-8.0f, -0.5f, -4.0f, 
		-8.0f, 2.5f, -4.0f,  

		-8.0f, -0.5f, 16.0f, 
		-8.0f, 2.5f, -4.0f,  
		-8.0f, 2.5f, 16.0f,  

		12.0f, -0.5f, -4.0f, 
		-8.0f, -0.5f, -4.0f, 
		-8.0f, 2.5f,  -4.0f, 

		12.0f, -0.5f, -4.0f, 
		-8.0f, 2.5f,  -4.0f, 
		12.0f, 2.5f,  -4.0f, 

		12.0f, -0.5f, -4.0f,  
		12.0f, -0.5f, -10.0f, 
		12.0f, 2.5f, -10.0f,  

		12.0f, -0.5f, -4.0f,  
		12.0f, 2.5f, -10.0f,  
		12.0f, 2.5f, -4.0f,   

		12.0f, -0.5f, -10.0f, 
		2.0f, -0.5f,  -10.0f, 
		2.0f, 2.5f,   -10.0f, 

		12.0f, -0.5f, -10.0f, 
		2.0f, 2.5f,   -10.0f, 
		12.0f, 2.5f,  -10.0f, 

		24.0f, -0.5f,  -14.0f,
		-12.0f, -0.5f, -14.0f,
		-12.0f, 2.5f,  -14.0f,

		24.0f, -0.5f,  -14.0f,
		-12.0f, 2.5f,  -14.0f,
		24.0f, 2.5f,  -14.0f,

		24.0f, -0.5f, -14.0f,
		24.0f, -0.5f, -20.0f,
		24.0f, 2.5f, -20.0f, 

		24.0f, -0.5f, -14.0f,
		24.0f, 2.5f, -20.0f,
		24.0f, 2.5f, -14.0f,

		60.0f, -0.5f, -6.0f,  
		12.0f, -0.5f, -6.0f,  
		12.0f, 2.5f, -6.0f,   

		60.0f, -0.5f, -6.0f,  
		12.0f, 2.5f, -6.0f,   
		60.0f, 2.5f, -6.0f,   

		30.0f, -0.5f, -6.0f,  
		30.0f, -0.5f, -12.0f, 
		30.0f, 2.5f, -12.0f,  

		30.0f, -0.5f, -6.0f,  
		30.0f, 2.5f, -12.0f,  
		30.0f, 2.5f, -6.0f,   

		44.0f, -0.5f, -12.0f, 
		30.0f, -0.5f, -12.0f, 
		30.0f, 2.5f, -12.0f,  

		44.0f, -0.5f, -12.0f, 
		30.0f, 2.5f, -12.0f,  
		44.0f, 2.5f, -12.0f,  

		50.0f, -0.5f, -12.0f, 
		50.0f, -0.5f, -20.0f, 
		50.0f, 2.5f, -20.0f,  

		50.0f, -0.5f, -12.0f, 
		50.0f, 2.5f, -20.0f,  
		50.0f, 2.5f, -12.0f,  

		54.0f, -0.5f, -12.0f, 
		50.0f, -0.5f, -12.0f, 
		50.0f, 2.5f, -12.0f,  

		54.0f, -0.5f, -12.0f,
		50.0f, 2.5f, -12.0f,
		54.0f, 2.5f, -12.0f,
		};

		glm::vec3 oldPosition = position;
		float velocity = (float)(cameraSpeedFactor * deltaTime*5);
		switch (direction) {
		case ECameraMovementType::FORWARD:
			position += forward * velocity;
			break;
		case ECameraMovementType::BACKWARD:
			position -= forward * velocity;
			break;
		case ECameraMovementType::LEFT:
			position -= right * velocity;
			break;
		case ECameraMovementType::RIGHT:
			position += right * velocity;
			break;
		case ECameraMovementType::UP:
			position += up * velocity;
			break;
		case ECameraMovementType::DOWN:
			position -= up * velocity;
			break;
		}
		int index=0;
		while (index < wallsCoord.size())
		{
			float xm = wallsCoord.at(index);
			float xM = wallsCoord.at(index);
			float zm = wallsCoord.at(index + 2);
			float zM = wallsCoord.at(index + 2);
			index += 3;
			for (int i = 0; i < 5; i++)
			{
				float x = wallsCoord.at(index);
				if (x < xm)
					xm = x;
				if (x > xM)
					xM = x;
				float z = wallsCoord.at(index + 2);
				if (z < zm)
					zm = z;
				if (z > zM)
					zM = z;
				index += 3;
			}
			if (verificaColiziune(position, oldPosition, xm, xM, zm, zM))
			{
				if (oldPosition.z != position.z)
					if (oldPosition.z < position.z)
						oldPosition.z -= 0.5f;
					else
						oldPosition.z += 0.5f;

				if (oldPosition.x != position.x)
					if (oldPosition.x < position.x)
						oldPosition.x -= 0.5f;
					else
						oldPosition.x += 0.5f;

				position = oldPosition;
				break;
			}
		}
		float y = position.y;
		if (y < -1.5f || y > 4.0f)
			position = oldPosition;
	}

	void MouseControl(float xPos, float yPos)
	{
		if (bFirstMouseMove) {
			lastX = xPos;
			lastY = yPos;
			bFirstMouseMove = false;
		}

		float xChange = xPos - lastX;
		float yChange = lastY - yPos;
		lastX = xPos;
		lastY = yPos;

		if (fabs(xChange) <= 1e-6 && fabs(yChange) <= 1e-6) {
			return;
		}
		xChange *= mouseSensitivity;
		yChange *= mouseSensitivity;

		ProcessMouseMovement(xChange, yChange);
	}

	void ProcessMouseScroll(float yOffset)
	{
		if (FoVy >= 1.0f && FoVy <= 90.0f) {
			FoVy -= yOffset;
		}
		if (FoVy <= 1.0f)
			FoVy = 1.0f;
		if (FoVy >= 90.0f)
			FoVy = 90.0f;
	}

private:
	void ProcessMouseMovement(float xOffset, float yOffset, bool constrainPitch = true)
	{
		yaw += xOffset;
		pitch += yOffset;

		// Se modific� vectorii camerei pe baza unghiurilor Euler
		UpdateCameraVectors();
	}

	void UpdateCameraVectors()
	{
		// Calculate the new forward vector
		this->forward.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		this->forward.y = sin(glm::radians(pitch));
		this->forward.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		this->forward = glm::normalize(this->forward);
		// Also re-calculate the Right and Up vector
		right = glm::normalize(glm::cross(forward, worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		up = glm::normalize(glm::cross(right, forward));
	}

protected:
	const float cameraSpeedFactor = 2.5f;
	const float mouseSensitivity = 0.1f;

	// Perspective properties
	float zNear;
	float zFar;
	float FoVy;
	int width;
	int height;
	bool isPerspective;

	glm::vec3 position;
	glm::vec3 forward;
	glm::vec3 right;
	glm::vec3 up;
	glm::vec3 worldUp;

	// Euler Angles
	float yaw;
	float pitch;

	bool bFirstMouseMove = true;
	float lastX = 0.f, lastY = 0.f;
};

class Shader
{
public:
	// constructor generates the shader on the fly
	Shader(const char* vertexPath, const char* fragmentPath)
	{
		Init(vertexPath, fragmentPath);
	}

	~Shader()
	{
		glDeleteProgram(ID);
	}

	// activate the shader
	void Use() const
	{
		glUseProgram(ID);
	}

	unsigned int GetID() const { return ID; }

	// MVP
	unsigned int loc_model_matrix;
	unsigned int loc_view_matrix;
	unsigned int loc_projection_matrix;

	// utility uniform functions
	void SetInt(const std::string &name, int value) const
	{
		glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
	}
	void SetFloat(const std::string &name, const float &value) const
	{
		glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
	}
	void SetVec3(const std::string &name, const glm::vec3 &value) const
	{
		glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
	}
	void SetVec3(const std::string &name, float x, float y, float z) const
	{
		glUniform3f(glGetUniformLocation(ID, name.c_str()), x, y, z);
	}
	void SetMat4(const std::string &name, const glm::mat4 &mat) const
	{
		glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
	}

private:
	void Init(const char* vertexPath, const char* fragmentPath)
	{
		// 1. retrieve the vertex/fragment source code from filePath
		std::string vertexCode;
		std::string fragmentCode;
		std::ifstream vShaderFile;
		std::ifstream fShaderFile;
		// ensure ifstream objects can throw exceptions:
		vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		try {
			// open files
			vShaderFile.open(vertexPath);
			fShaderFile.open(fragmentPath);
			std::stringstream vShaderStream, fShaderStream;
			// read file's buffer contents into streams
			vShaderStream << vShaderFile.rdbuf();
			fShaderStream << fShaderFile.rdbuf();
			// close file handlers
			vShaderFile.close();
			fShaderFile.close();
			// convert stream into string
			vertexCode = vShaderStream.str();
			fragmentCode = fShaderStream.str();
		}
		catch (std::ifstream::failure e) {
			std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
		}
		const char* vShaderCode = vertexCode.c_str();
		const char * fShaderCode = fragmentCode.c_str();

		// 2. compile shaders
		unsigned int vertex, fragment;
		// vertex shader
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vShaderCode, NULL);
		glCompileShader(vertex);
		CheckCompileErrors(vertex, "VERTEX");
		// fragment Shader
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fShaderCode, NULL);
		glCompileShader(fragment);
		CheckCompileErrors(fragment, "FRAGMENT");
		// shader Program
		ID = glCreateProgram();
		glAttachShader(ID, vertex);
		glAttachShader(ID, fragment);
		glLinkProgram(ID);
		CheckCompileErrors(ID, "PROGRAM");

		// 3. delete the shaders as they're linked into our program now and no longer necessery
		glDeleteShader(vertex);
		glDeleteShader(fragment);
	}

	// utility function for checking shader compilation/linking errors.
	void CheckCompileErrors(unsigned int shader, std::string type)
	{
		GLint success;
		GLchar infoLog[1024];
		if (type != "PROGRAM") {
			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
			if (!success) {
				glGetShaderInfoLog(shader, 1024, NULL, infoLog);
				std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
			}
		}
		else {
			glGetProgramiv(shader, GL_LINK_STATUS, &success);
			if (!success) {
				glGetProgramInfoLog(shader, 1024, NULL, infoLog);
				std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
			}
		}
	}
private:
	unsigned int ID;
};


Camera *pCamera = nullptr;

void Cleanup()
{
	delete pCamera;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

// timing
double deltaTime = 0.0f;	// time between current frame and last frame
double lastFrame = 0.0f;

unsigned int CreateTexture(const std::string& strTexturePath);
unsigned int alleyVAO = 0;
void renderAlley()
{
	if (alleyVAO == 0)
	{
		float alleyVertices[] = {
			// positions            // texcoords
			 100.0f, -0.51f, 100.0f,    1.0f,  0.0f,
			-60.0f, -0.51f, 100.0f,    0.0f,  0.0f,
			-60.0f, -0.51f, -60.0f,    0.0f,  1.0f,

			 100.0f, -0.51f, 100.0f,    1.0f,  0.0f,
			-60.0f, -0.51f, -60.0f,    0.0f,  1.0f,
			 100.0f, -0.51f, -60.0f,    1.0f,  1.0f
		};
		// plane VAO&VBO
		unsigned int alleyVBO;
		glGenVertexArrays(1, &alleyVAO);
		glGenBuffers(1, &alleyVBO);
		glBindVertexArray(alleyVAO);
		glBindBuffer(GL_ARRAY_BUFFER, alleyVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(alleyVertices), alleyVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}
	//render alley
	glBindVertexArray(alleyVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}
unsigned int floorVAO = 0;
void renderFloor()
{
	if (floorVAO == 0)
	{
		float floorVertices[] = {
			// positions            // texcoords
			 5.0f, -0.5f,  5.0f,    1.0f,  0.0f,
			-5.0f, -0.5f,  5.0f,    0.0f,  0.0f,
			-5.0f, -0.5f, -5.0f,    0.0f,  1.0f,

			 5.0f, -0.5f,  5.0f,    1.0f,  0.0f,
			-5.0f, -0.5f, -5.0f,    0.0f,  1.0f,
			 5.0f, -0.5f, -5.0f,    1.0f,  1.0f
		};
		// plane VAO&VBO
		unsigned int floorVBO;
		glGenVertexArrays(1, &floorVAO);
		glGenBuffers(1, &floorVBO);
		glBindVertexArray(floorVAO);
		glBindBuffer(GL_ARRAY_BUFFER, floorVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(floorVertices), floorVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	//render floor
	glBindVertexArray(floorVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}
unsigned int wallVAO = 0;
void renderWall()
{
	if (wallVAO==0)
	{
		float wallVertices[] = {
			// positions    // texcoords
			-5.0f, 2.5f,  -5.0f,   1.0f, 1.0f,
			-5.0f, -0.5f,  5.0f,   0.0f, 0.0f,
			-5.0f, -0.5f, -5.0f,   1.0f, 0.0f,

			-5.0f, 2.5f,  -5.0f,  1.0f, 1.0f,
			-5.0f, 2.5f,  5.0f,  0.0f, 1.0f,
			-5.0f, -0.5f, 5.0f,  0.0f, 0.0f
		};
		// plane VAO&VBO
		unsigned int  wallVBO;
		glGenVertexArrays(1, &wallVAO);
		glGenBuffers(1, &wallVBO);
		glBindVertexArray(wallVAO);
		glBindBuffer(GL_ARRAY_BUFFER, wallVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(wallVertices), wallVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	//render wall
	glBindVertexArray(wallVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}
unsigned int insideWallVAO = 0;
void renderWallsInside()
{

	if (insideWallVAO == 0)
	{
		float wallsVertices[] = {
			// positions    // texcoords
			22.0f, -0.5f,  60.0f,   0.0f, 1.0f,
			22.0f, -0.5f,  56.0f,   1.0f, 1.0f,
			22.0f, 2.5f,  60.0f,   0.0f, 0.0f,

			22.0f, 2.5f,  60.0f,   0.0f, 0.0f,
			22.0f, 2.5f,  56.0f,  1.0f, 0.0f,
			22.0f, -0.5f, 56.0f,  1.0f, 1.0f,


			22.0f, -0.5f,  56.0f,   1.0f, 1.0f,
			12.0f, -0.5f,  56.0f, 0.0f, 1.0f,
			22.0f,  2.5f,  56.0f, 1.0f, 0.0f,

			22.0f, 2.5f,  56.0f,   1.0f, 0.0f,
			12.0f, 2.5f,  56.0f,  0.0f, 0.0f,
			12.0f,  -0.5f, 56.0f, 0.0f, 1.0f,

			19.0f, -0.5f, 56.0f,  1.0f, 0.0f,
			19.0f, -0.5f, 50.0f, 0.0f, 0.0f,
			19.0f, 2.5f, 50.0f,  0.0f, 1.0f,

			19.0f, -0.5f, 56.0f,  1.0f, 0.0f,
			19.0f, 2.5f, 50.0f,  0.0f, 1.0f,
			19.0f, 2.5f, 56.0f,  1.0f, 1.0f,

			6.0f, -0.5f, 60.0f,  1.0f, 0.0f,
			6.0f, -0.5f, 50.0f,  0.0f, 0.0f,
			6.0f, 2.5f, 50.0f,   0.0f, 1.0f,

			6.0f, -0.5f, 60.0f,  1.0f, 0.0f,
			6.0f, 2.5f, 50.0f,   0.0f, 1.0f,
			6.0f, 2.5f, 60.0f,   1.0f, 1.0f,

			24.0f, -0.5f, 60.0f,  1.0f, 0.0f,
			24.0f, -0.5f, 38.0f,  0.0f, 0.0f,
			24.0f, 2.5f, 38.0f,   0.0f, 1.0f,

			24.0f, -0.5f, 60.0f,  1.0f, 0.0f,
			24.0f, 2.5f, 38.0f,   0.0f, 1.0f,
			24.0f, 2.5f, 60.0f,   1.0f, 1.0f,

			24.0f, -0.5f, 38.0f,  1.0f, 0.0f,
			14.0f, -0.5f, 38.0f,  0.0f, 0.0f,
			14.0f, 2.5f, 38.0f,   0.0f, 1.0f,

			24.0f, -0.5f, 38.0f,  1.0f, 0.0f,
			14.0f, 2.5f, 38.0f,   0.0f, 1.0f,
			24.0f, 2.5f, 38.0f,   1.0f, 1.0f,

			10.0f, -0.5f, 38.0f,  1.0f, 0.0f,
			-20.0f, -0.5f, 38.0f, 0.0f, 0.0f,
			-20.0f, 2.5f, 38.0f,  0.0f, 1.0f,

			10.0f, -0.5f, 38.0f,  1.0f, 0.0f,
			-20.0f, 2.5f, 38.0f,  0.0f, 1.0f,
			10.0f, 2.5f, 38.0f,   1.0f, 1.0f,

			12.0f, -0.5f, 44.0f,  1.0f, 0.0f,
			-8.0f, -0.5f, 44.0f,  0.0f, 0.0f,
			-8.0f, 2.5f, 44.0f,   0.0f, 1.0f,

			12.0f, -0.5f, 44.0f,  1.0f, 0.0f,
			-8.0f, 2.5f, 44.0f,   0.0f, 1.0f,
			12.0f, 2.5f, 44.0f,   1.0f, 1.0f,

			36.0f, -0.5f, 26.0f,  1.0f, 0.0f,
			-20.0f, -0.5f, 26.0f, 0.0f, 0.0f,
			-20.0f, 2.5f, 26.0f,  0.0f, 1.0f,

			36.0f, -0.5f, 26.0f,  1.0f, 0.0f,
			-20.0f, 2.5f, 26.0f,  0.0f, 1.0f,
			36.0f, 2.5f, 26.0f,   1.0f, 1.0f,

			18.0f, -0.5f, 26.0f,  1.0f, 0.0f,
			18.0f, -0.5f, 2.0f,   0.0f, 0.0f,
			18.0f, 2.5f, 2.0f,    0.0f, 1.0f,

			18.0f, -0.5f, 26.0f,  1.0f, 0.0f,
			18.0f, 2.5f, 2.0f,    0.0f, 1.0f,
			18.0f, 2.5f, 26.0f,   1.0f, 1.0f,

			26.0f, -0.5f, 2.0f,   1.0f, 0.0f,
			18.0f, -0.5f, 2.0f,   0.0f, 0.0f,
			18.0f, 2.5f, 2.0f,    0.0f, 1.0f,

			26.0f, -0.5f, 2.0f,   1.0f, 0.0f,
			18.0f, 2.5f, 2.0f,    0.0f, 1.0f,
			26.0f, 2.5f, 2.0f,    1.0f, 1.0f,

			30.0f, -0.5f, 54.0f,  1.0f, 0.0f,
			30.0f, -0.5f, 48.0f,  0.0f, 0.0f,
			30.0f, 2.5f, 48.0f,   0.0f, 1.0f,

			30.0f, -0.5f, 54.0f,  1.0f, 0.0f,
			30.0f, 2.5f, 48.0f,   0.0f, 1.0f,
			30.0f, 2.5f, 54.0f,   1.0f, 1.0f,

			36.0f, -0.5f, 54.0f,  1.0f, 0.0f,
			30.0f, -0.5f, 54.0f,  0.0f, 0.0f,
			30.0f, 2.5f, 54.0f,   0.0f, 1.0f,

			36.0f, -0.5f, 54.0f,  1.0f, 0.0f,
			30.0f, 2.5f, 54.0f,   0.0f, 1.0f,
			36.0f, 2.5f, 54.0f,   1.0f, 1.0f,

			44.0f, -0.5f, 60.0f,  1.0f, 0.0f,
			44.0f, -0.5f, 56.0f,  0.0f, 0.0f,
			44.0f, 2.5f, 56.0f,   0.0f, 1.0f,

			44.0f, -0.5f, 60.0f,  1.0f, 0.0f,
			44.0f, 2.5f, 56.0f,   0.0f, 1.0f,
			44.0f, 2.5f, 60.0f,   1.0f, 1.0f,

			30.0f, -0.5f, 42.0f,  1.0f, 0.0f,
			30.0f, -0.5f, 36.0f,  0.0f, 0.0f,
			30.0f, 2.5f, 36.0f,   0.0f, 1.0f,

			30.0f, -0.5f, 42.0f,  1.0f, 0.0f,
			30.0f, 2.5f, 36.0f,   0.0f, 1.0f,
			30.0f, 2.5f, 42.0f,   1.0f, 1.0f,

			40.0f, -0.5f, 42.0f, 1.0f, 0.0f,
			30.0f, -0.5f, 42.0f, 0.0f, 0.0f,
			30.0f, 2.5f, 42.0f,  0.0f, 1.0f,

			40.0f, -0.5f, 42.0f, 1.0f, 0.0f,
			30.0f, 2.5f, 42.0f,  0.0f, 1.0f,
			40.0f, 2.5f, 42.0f,  1.0f, 1.0f,

			40.0f, -0.5f, 50.0f, 1.0f, 0.0f,
			40.0f, -0.5f, 42.0f, 0.0f, 0.0f,
			40.0f, 2.5f, 42.0f,  0.0f, 1.0f,

			40.0f, -0.5f, 50.0f, 1.0f, 0.0f,
			40.0f, 2.5f,  42.0f, 0.0f, 1.0f,
			40.0f, 2.5f, 50.0f,  1.0f, 1.0f,

			55.0f, -0.5f, 50.0f, 1.0f, 0.0f,
			40.0f, -0.5f, 50.0f, 0.0f, 0.0f,
			40.0f, 2.5f, 50.0f,  0.0f, 1.0f,

			55.0f, -0.5f, 50.0f, 1.0f, 0.0f,
			40.0f, 2.5f, 50.0f,  0.0f, 1.0f,
			55.0f, 2.5f, 50.0f,  1.0f, 1.0f,

			50.0f, -0.5f, 50.0f, 1.0f, 0.0f,
			50.0f, -0.5f, 8.0f,  0.0f, 0.0f,
			50.0f, 2.5f, 8.0f,   0.0f, 1.0f,

			50.0f, -0.5f, 50.0f, 1.0f, 0.0f,
			50.0f, 2.5f, 8.0f,   0.0f, 1.0f,
			50.0f, 2.5f, 50.0f,  1.0f, 1.0f,

			55.0f, -0.5f, 32.0f, 1.0f, 0.0f,
			50.0f, -0.5f, 32.0f, 0.0f, 0.0f,
			50.0f, 2.5f, 32.0f,  0.0f, 1.0f,

			55.0f, -0.5f, 32.0f, 1.0f, 0.0f,
			50.0f, 2.5f, 32.0f,  0.0f, 1.0f,
			55.0f, 2.5f, 32.0f,  1.0f, 1.0f,

			44.0f, -0.5f, 38.0f, 1.0f, 0.0f,
			44.0f, -0.5f, 22.0f, 0.0f, 0.0f,
			44.0f, 2.5f, 22.0f,  0.0f, 1.0f,

			44.0f, -0.5f, 38.0f, 1.0f, 0.0f,
			44.0f, 2.5f, 22.0f,  0.0f, 1.0f,
			44.0f, 2.5f, 38.0f,  1.0f, 1.0f,

			54.0f, -0.5f, 22.0f, 1.0f, 0.0f,
			44.0f, -0.5f, 22.0f, 0.0f, 0.0f,
			44.0f, 2.5f, 22.0f,  0.0f, 1.0f,

			54.0f, -0.5f, 22.0f, 1.0f, 0.0f,
			44.0f, 2.5f, 22.0f,  0.0f, 1.0f,
			54.0f, 2.5f, 22.0f,  1.0f, 1.0f,

			60.0f, -0.5f, 12.0f, 1.0f, 0.0f,
			55.0f, -0.5f, 12.0f, 0.0f, 0.0f,
			55.0f, 2.5f, 12.0f,  0.0f, 1.0f,

			60.0f, -0.5f, 12.0f, 1.0f, 0.0f,
			55.0f, 2.5f, 12.0f,  0.0f, 1.0f,
			60.0f, 2.5f, 12.0f,  1.0f, 1.0f,

			60.0f, -0.5f, 2.0f, 1.0f, 0.0f,
			48.0f, -0.5f, 2.0f, 0.0f, 0.0f,
			48.0f, 2.5f, 2.0f,  0.0f, 1.0f,

			60.0f, -0.5f, 2.0f, 1.0f, 0.0f,
			48.0f, 2.5f, 2.0f,  0.0f, 1.0f,
			60.0f, 2.5f, 2.0f,  1.0f, 1.0f,

			42.0f, -0.5f, 4.0f,  1.0f, 0.0f,
			32.0f, -0.5f, 4.0f,  0.0f, 0.0f,
			32.0f, 2.5f, 4.0f,   0.0f, 1.0f,

			42.0f, -0.5f, 4.0f,  1.0f, 0.0f,
			32.0f, 2.5f, 4.0f,   0.0f, 1.0f,
			42.0f, 2.5f, 4.0f,   1.0f, 1.0f,

			32.0f, -0.5f, 4.0f,  1.0f, 0.0f,
			32.0f, -0.5f, 10.0f, 0.0f, 0.0f,
			32.0f, 2.5f, 10.0f,  0.0f, 1.0f,

			32.0f, -0.5f, 4.0f,  1.0f, 0.0f,
			32.0f, 2.5f, 10.0f,  0.0f, 1.0f,
			32.0f, 2.5f, 4.0f,   1.0f, 1.0f,

			32.0f, -0.5f, 10.0f, 1.0f, 0.0f,
			24.0f, -0.5f, 10.0f, 0.0f, 0.0f,
			24.0f, 2.5f, 10.0f,  0.0f, 1.0f,

			32.0f, -0.5f, 10.0f, 1.0f, 0.0f,
			24.0f, 2.5f, 10.0f,  0.0f, 1.0f,
			32.0f, 2.5f, 10.0f,  1.0f, 1.0f,

			24.0f, -0.5f, 26.0f, 1.0f, 0.0f,
			24.0f, -0.5f, 10.0f, 0.0f, 0.0f,
			24.0f, 2.5f, 10.0f,  0.0f, 1.0f,

			24.0f, -0.5f, 26.0f, 1.0f, 0.0f,
			24.0f, 2.5f, 10.0f,  0.0f, 1.0f,
			24.0f, 2.5f, 26.0f,  1.0f, 1.0f,

			32.0f, -0.5f, 18.0f, 1.0f, 0.0f,
			24.0f, -0.5f,18.0f,  0.0f, 0.0f,
			24.0f, 2.5f, 18.0f,  0.0f, 1.0f,


			32.0f, -0.5f,18.0f,  1.0f, 0.0f,
			24.0f, 2.5f, 18.0f,  0.0f, 1.0f,
			32.0f, 2.5f, 18.0f,  1.0f, 1.0f,

			40.0f, -0.5f, 18.0f, 1.0f, 0.0f,
			40.0f, -0.5f, 8.0f,  0.0f, 0.0f,
			40.0f, 2.5f,  8.0f,  0.0f, 1.0f,

			40.0f, -0.5f, 18.0f, 1.0f, 0.0f,
			40.0f, 2.5f,  8.0f,  0.0f, 1.0f,
			40.0f, 2.5f,  18.0f, 1.0f, 1.0f,

			-4.0f, -0.5f, 26.0f, 1.0f, 0.0f,
			-4.0f, -0.5f, 18.0f, 0.0f, 0.0f,
			-4.0f, 2.5f, 18.0f,  0.0f, 1.0f,

			-4.0f, -0.5f, 26.0f, 1.0f, 0.0f,
			-4.0f, 2.5f, 18.0f,  0.0f, 1.0f,
			-4.0f, 2.5f, 26.0f,  1.0f, 1.0f,

			8.0f, -0.5f, 18.0f,  1.0f, 0.0f,
			-4.0f, -0.5f, 18.0f, 0.0f, 0.0f,
			-4.0f, 2.5f, 18.0f,  0.0f, 1.0f,

			8.0f, -0.5f, 18.0f, 1.0f, 0.0f,
			-4.0f, 2.5f, 18.0f, 0.0f, 1.0f,
			8.0f, 2.5f, 18.0f,  1.0f, 1.0f,

			8.0f, -0.5f, 18.0f, 1.0f, 0.0f,
			8.0f, -0.5f, 6.0f,  0.0f, 0.0f,
			8.0f, 2.5f, 6.0f,   0.0f, 1.0f,

			8.0f, -0.5f, 18.0f, 1.0f, 0.0f,
			8.0f, 2.5f, 6.0f,   0.0f, 1.0f,
			8.0f, 2.5f, 18.0f,  1.0f, 1.0f,

			-2.0f, -0.5f, 18.0f, 1.0f, 0.0f,
			-2.0f, -0.5f, 6.0f,  0.0f, 0.0f,
			-2.0f, 2.5f, 6.0f,   0.0f, 1.0f,

			-2.0f, -0.5f, 18.0f, 1.0f, 0.0f,
			-2.0f, 2.5f, 6.0f,   0.0f, 1.0f,
			-2.0f, 2.5f, 18.0f,  1.0f, 1.0f,

			-14.0f, -0.5f, 16.0f, 1.0f, 0.0f,
			-14.0f, -0.5f, -4.0f, 0.0f, 0.0f,
			-14.0f, 2.5f, -4.0f,  0.0f, 1.0f,

			-14.0f, -0.5f, 16.0f, 1.0f, 0.0f,
			-14.0f, 2.5f, -4.0f,  0.0f, 1.0f,
			-14.0f, 2.5f, 16.0f,  1.0f, 1.0f,

			-8.0f, -0.5f, 16.0f,  1.0f, 0.0f,
			-14.0f, -0.5f, 16.0f, 0.0f, 0.0f,
			-14.0f, 2.5f, 16.0f,  0.0f, 1.0f,

			-8.0f, -0.5f, 16.0f, 1.0f, 0.0f,
			-14.0f, 2.5f, 16.0f, 0.0f, 1.0f,
			-8.0f, 2.5f, 16.0f,  1.0f, 1.0f,

			-8.0f, -0.5f, 16.0f, 1.0f, 0.0f,
			-8.0f, -0.5f, -4.0f, 0.0f, 0.0f,
			-8.0f, 2.5f, -4.0f,  0.0f, 1.0f,

			-8.0f, -0.5f, 16.0f, 1.0f, 0.0f,
			-8.0f, 2.5f, -4.0f,  0.0f, 1.0f,
			-8.0f, 2.5f, 16.0f,  1.0f, 1.0f,

			12.0f, -0.5f, -4.0f, 1.0f, 0.0f,
			-8.0f, -0.5f, -4.0f, 0.0f, 0.0f,
			-8.0f, 2.5f,  -4.0f, 0.0f, 1.0f,

			12.0f, -0.5f, -4.0f, 1.0f, 0.0f,
			-8.0f, 2.5f,  -4.0f, 0.0f, 1.0f,
			12.0f, 2.5f,  -4.0f, 1.0f, 1.0f,

			12.0f, -0.5f, -4.0f,  1.0f, 0.0f,
			12.0f, -0.5f, -10.0f, 0.0f, 0.0f,
			12.0f, 2.5f, -10.0f,  0.0f, 1.0f,

			12.0f, -0.5f, -4.0f,  1.0f, 0.0f,
			12.0f, 2.5f, -10.0f,  0.0f, 1.0f,
			12.0f, 2.5f, -4.0f,   1.0f, 1.0f,

			12.0f, -0.5f, -10.0f, 1.0f, 0.0f,
			2.0f, -0.5f,  -10.0f, 0.0f, 0.0f,
			2.0f, 2.5f,   -10.0f, 0.0f, 1.0f,

			12.0f, -0.5f, -10.0f, 1.0f, 0.0f,
			2.0f, 2.5f,   -10.0f, 0.0f, 1.0f,
			12.0f, 2.5f,  -10.0f, 1.0f, 1.0f,

			24.0f, -0.5f,  -14.0f, 1.0f, 0.0f,
			-12.0f, -0.5f, -14.0f, 0.0f, 0.0f,
			-12.0f, 2.5f,  -14.0f, 0.0f, 1.0f,

			24.0f, -0.5f,  -14.0f, 1.0f, 0.0f,
			-12.0f, 2.5f,  -14.0f, 0.0f, 1.0f,
			24.0f, 2.5f,  -14.0f, 1.0f, 1.0f,

			24.0f, -0.5f, -14.0f, 1.0f, 0.0f,
			24.0f, -0.5f, -20.0f, 0.0f, 0.0f,
			24.0f, 2.5f, -20.0f,  0.0f, 1.0f,

			24.0f, -0.5f, -14.0f, 1.0f, 0.0f,
			24.0f, 2.5f, -20.0f,  0.0f, 1.0f,
			24.0f, 2.5f, -14.0f,  1.0f, 1.0f,

			60.0f, -0.5f, -6.0f,  1.0f, 0.0f,
			12.0f, -0.5f, -6.0f,  0.0f, 0.0f,
			12.0f, 2.5f, -6.0f,   0.0f, 1.0f,

			60.0f, -0.5f, -6.0f,  1.0f, 0.0f,
			12.0f, 2.5f, -6.0f,   0.0f, 1.0f,
			60.0f, 2.5f, -6.0f,   1.0f, 1.0f,

			30.0f, -0.5f, -6.0f,  1.0f, 0.0f,
			30.0f, -0.5f, -12.0f, 0.0f, 0.0f,
			30.0f, 2.5f, -12.0f,  0.0f, 1.0f,

			30.0f, -0.5f, -6.0f,  1.0f, 0.0f,
			30.0f, 2.5f, -12.0f,  0.0f, 1.0f,
			30.0f, 2.5f, -6.0f,   1.0f, 1.0f,

			44.0f, -0.5f, -12.0f, 1.0f, 0.0f,
			30.0f, -0.5f, -12.0f, 0.0f, 0.0f,
			30.0f, 2.5f, -12.0f,  0.0f, 1.0f,

			44.0f, -0.5f, -12.0f, 1.0f, 0.0f,
			30.0f, 2.5f, -12.0f,  0.0f, 1.0f,
			44.0f, 2.5f, -12.0f,  1.0f, 1.0f,

			50.0f, -0.5f, -12.0f, 1.0f, 0.0f,
			50.0f, -0.5f, -20.0f, 0.0f, 0.0f,
			50.0f, 2.5f, -20.0f,  0.0f, 1.0f,

			50.0f, -0.5f, -12.0f, 1.0f, 0.0f,
			50.0f, 2.5f, -20.0f,  0.0f, 1.0f,
			50.0f, 2.5f, -12.0f,  1.0f, 1.0f,

			54.0f, -0.5f, -12.0f, 1.0f, 0.0f,
			50.0f, -0.5f, -12.0f, 0.0f, 0.0f,
			50.0f, 2.5f, -12.0f,  0.0f, 1.0f,

			54.0f, -0.5f, -12.0f, 1.0f, 0.0f,
			50.0f, 2.5f, -12.0f,  0.0f, 1.0f,
			54.0f, 2.5f, -12.0f,  1.0f, 1.0f
		};

		// plane VAO&VBO
		unsigned int insideWallVBO;
		glGenVertexArrays(1, &insideWallVAO);
		glGenBuffers(1, &insideWallVBO);
		glBindVertexArray(insideWallVAO);
		glBindBuffer(GL_ARRAY_BUFFER, insideWallVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(wallsVertices), wallsVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
	}
	//render walls
	glBindVertexArray(insideWallVAO);
	glDrawArrays(GL_TRIANGLES, 0, 282);
	glBindVertexArray(0);
}
unsigned int frontWallVAO = 0;
void renderWallsFrontAndBack()
{
	if (frontWallVAO == 0)
	{
		float wallVertices[] = {
			// positions    // texcoords
			-5.0f, 2.5f,  -5.0f,   1.0f, 1.0f,
			-5.0f, -0.5f,  -5.0f,   1.0f, 0.0f,
			4.5f, -0.5f, -5.0f,   0.0f, 0.0f,

			-5.0f, 2.5f,  -5.0f,  1.0f, 1.0f,
			 4.5f, 2.5f,  -5.0f,  0.0f, 1.0f,
			 4.5f, -0.5f, -5.0f,  0.0f, 0.0f,

			4.5f, 2.5f,  -5.0f,  1.0f, 1.0f,
			4.5f, 0.5f,  -5.0f,  0.0f, 1.0f,
			5.0f, 2.5f, -5.0f,  0.0f, 0.0f,

			5.0f, 2.5f,  -5.0f,  1.0f, 1.0f,
			4.5f, 0.5f,  -5.0f,  0.0f, 1.0f,
			5.0f, 0.5f, -5.0f,  0.0f, 0.0f


		};
		// plane VAO&VBO
		unsigned int  wallVBO;
		glGenVertexArrays(1, &frontWallVAO);
		glGenBuffers(1, &wallVBO);
		glBindVertexArray(frontWallVAO);
		glBindBuffer(GL_ARRAY_BUFFER, wallVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(wallVertices), wallVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	//render walls
	glBindVertexArray(frontWallVAO);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	glBindVertexArray(0);
}
unsigned int roofVAO = 0;
void renderRoof()
{ //COORDONATELE TEXTURILOR
	if (roofVAO == 0)
	{
		float roofVertices[] = {
			// positions    // texcoords
			60.0f, 10.0f,  60.0f,   0.0f, 1.0f,
			-20.0f, 10.0f, 60.0f,   1.0f, 0.0f,
			20.0f, 40.0f, 20.0f,   0.0f, 0.0f,

			60.0f, 10.0f,  -20.0f,  1.0f, 1.0f,//ok
			-20.0f, 10.0f, -20.0f,  0.0f, 1.0f,
			 20.0f, 40.0f, 20.0f,  0.0f, 0.0f,

			60.0f, 10.0f,  -20.0f, 1.0f, 1.0f,//ok
			60.0f, 10.0f,  60.0f,  0.0f, 1.0f,
			20.0f, 40.0f, 20.0f,   0.0f, 0.0f,

			-20.0f, 10.0f,  -20.0f,  0.0f, 1.0f,
			-20.0f, 10.0f,  60.0f,  1.0f, 0.0f,
			20.0f, 40.0f, 20.0f,   0.0f, 0.0f,

			60.0f, 10.0f,  60.0f,   1.0f, 0.0f,
			-20.0f, 10.0f, 60.0f,   0.0f, 0.0f,
			-20.0f, 10.0f, -20.0f,   0.0f, 1.0f,

			60.0f, 10.0f,  60.0f,   1.0f, 0.0f,
			-20.0f, 10.0f, -20.0f,   0.0f, 1.0f,
			60.0f, 10.0f, -20.0f,   1.0f, 1.0f,
		};
		// plane VAO&VBO
		unsigned int wallVBO;
		glGenVertexArrays(1, &roofVAO);
		glGenBuffers(1, &wallVBO);
		glBindVertexArray(roofVAO);
		glBindBuffer(GL_ARRAY_BUFFER, wallVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(roofVertices), roofVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	//render roof
	glBindVertexArray(roofVAO);
	glDrawArrays(GL_TRIANGLES, 0, 18);
	glBindVertexArray(0);
}

int main(int argc, char** argv)
{
	std::string strFullExeFileName = argv[0];
	std::string strExePath;
	const size_t last_slash_idx = strFullExeFileName.rfind('\\');
	if (std::string::npos != last_slash_idx) {
		strExePath = strFullExeFileName.substr(0, last_slash_idx);
	}


	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Labirint", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewInit();

	glEnable(GL_DEPTH_TEST);

	// Floor texture
	unsigned int floorTexture = CreateTexture(strExePath + "\\ColoredFloor.png");
	unsigned int winter = CreateTexture(strExePath + "\\footprints.jpg");
	unsigned int floorTexture3 = CreateTexture(strExePath + "\\Stones.jpg");
	unsigned int roofTexture = CreateTexture(strExePath + "\\roof.jpg");
	unsigned int summer = CreateTexture(strExePath + "\\summer.jpg");
	unsigned int innerWalls = CreateTexture(strExePath + "\\wall.jpg");
	unsigned int extWalls = CreateTexture(strExePath + "\\wallsExt.jpg");
	unsigned int autumn = CreateTexture(strExePath + "\\autumn.jpg");
	unsigned int spring = CreateTexture(strExePath + "\\spring.jpg");
	unsigned int alley = CreateTexture(strExePath + "\\alley.jpg");

	// Create camera
	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(20.0f, 0.0f, 60.0f));
	glm::vec3 lightPos(pCamera->GetPosition());

	Shader lightingShader("PhongLight.vs", "PhongLight.fs");
	Shader shaderBlending("blending.vs", "blending.fs");
	shaderBlending.SetInt("texture1", 0);

	// render loop
	while (!glfwWindowShouldClose(window)) {
		// per-frame time logic
		double currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		processInput(window);
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		shaderBlending.Use();
		glm::mat4 projection = pCamera->GetProjectionMatrix();
		glm::mat4 view = pCamera->GetViewMatrix();
		shaderBlending.SetMat4("projection", projection);
		shaderBlending.SetMat4("view", view);

		glm::mat4 model;

		//Draw alley
		glBindTexture(GL_TEXTURE_2D, alley);
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		shaderBlending.SetMat4("model", model);
		renderAlley();

		//---------------- Draw floor-----------------------
		
		glBindTexture(GL_TEXTURE_2D, spring); //stanga sus
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		shaderBlending.SetMat4("model", model);
		renderFloor();	

		glBindTexture(GL_TEXTURE_2D, winter); //stanga jos
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 10.0f));
		shaderBlending.SetMat4("model", model);
		renderFloor();
		
		glBindTexture(GL_TEXTURE_2D, autumn);//dreapta sus
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(10.0f, 0.0f, 0.0f));
		shaderBlending.SetMat4("model", model);
		renderFloor();
		
		glBindTexture(GL_TEXTURE_2D, summer);//dreapta jos
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(10.0f, 0.0f, 10.0f));
		shaderBlending.SetMat4("model", model);
		renderFloor();	
		
		glBindTexture(GL_TEXTURE_2D, extWalls);//perete stanga-sus
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		shaderBlending.SetMat4("model", model);
		renderWall();
		
		glBindTexture(GL_TEXTURE_2D, extWalls);//perete stanga-jos
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 10.0f));
		shaderBlending.SetMat4("model", model);
		renderWall();
		
		glBindTexture(GL_TEXTURE_2D, extWalls);//perete dreapta-jos
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(20.0f, 0.0f, 10.0f));
		shaderBlending.SetMat4("model", model);
		renderWall();
		
		glBindTexture(GL_TEXTURE_2D, extWalls);//perete dreapta-sus
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(20.0f, 0.0f, 0.0f));
		shaderBlending.SetMat4("model", model);
		renderWall();
		
		glBindTexture(GL_TEXTURE_2D, extWalls);//perete usa stanga spate
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		shaderBlending.SetMat4("model", model);
		renderWallsFrontAndBack();
		
		glBindTexture(GL_TEXTURE_2D, extWalls);//perete usa dreapta spate
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 20.0f));
		shaderBlending.SetMat4("model", model);
		renderWallsFrontAndBack();
		
		glBindTexture(GL_TEXTURE_2D, extWalls);//perete usa stanga fata
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(10.0f, 0.0f, -10.0f));
		model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0, 1, 0));
		shaderBlending.SetMat4("model", model);
		renderWallsFrontAndBack();

		glBindTexture(GL_TEXTURE_2D, extWalls);//perete usa dreapta fata
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(4.0f));
		model = glm::translate(model, glm::vec3(10.0f, 0.0f, 10.0f));
		model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0, 1, 0));
		shaderBlending.SetMat4("model", model);
		renderWallsFrontAndBack();

		glBindTexture(GL_TEXTURE_2D, innerWalls);//pereti interiori
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(1.0f,4.0f,1.0f));
		shaderBlending.SetMat4("model", model);
		renderWallsInside();
		
		glBindTexture(GL_TEXTURE_2D, roofTexture);//acoperis
		model = glm::mat4();
		shaderBlending.SetMat4("model", model);
		renderRoof();



		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	Cleanup();

	glfwTerminate();
	return 0;
}


// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if ((glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) || (glfwGetKey(window, GLFW_KEY_W == GLFW_PRESS)))
		pCamera->ProcessKeyboard(FORWARD, (float)deltaTime);
	if ((glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) || (glfwGetKey(window, GLFW_KEY_S == GLFW_PRESS)))
		pCamera->ProcessKeyboard(BACKWARD, (float)deltaTime);
	if ((glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) || (glfwGetKey(window, GLFW_KEY_A == GLFW_PRESS)))
		pCamera->ProcessKeyboard(LEFT, (float)deltaTime);
	if ((glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) || (glfwGetKey(window, GLFW_KEY_D == GLFW_PRESS)))
		pCamera->ProcessKeyboard(RIGHT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
		pCamera->ProcessKeyboard(UP, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
		pCamera->ProcessKeyboard(DOWN, (float)deltaTime);

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		pCamera->Reset(width, height);

	}
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	pCamera->Reshape(width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	pCamera->MouseControl((float)xpos, (float)ypos);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yOffset)
{
	pCamera->ProcessMouseScroll((float)yOffset);
}

unsigned int CreateTexture(const std::string& strTexturePath)
{
	unsigned int textureId = -1;

	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	//stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char *data = stbi_load(strTexturePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		std::cout << "Failed to load texture: " << strTexturePath << std::endl;
	}
	stbi_image_free(data);

	return textureId;
}
